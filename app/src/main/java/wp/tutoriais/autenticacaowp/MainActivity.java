package wp.tutoriais.autenticacaowp;

import static androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG;
import static androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;
import com.google.android.material.snackbar.Snackbar;
import java.util.concurrent.Executor;

public class MainActivity extends AppCompatActivity {
    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Verificar se o dispositivo possui leitor de impressão digital
        BiometricManager sensorImpressao = BiometricManager.from(this);

        //Verificar se estão cadastrados ao menos a impressão digital
        switch (sensorImpressao.canAuthenticate(BIOMETRIC_STRONG)) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                //Caso possua o sensor e há ao menos uma digital cadastrada para uso
                Toast.makeText(MainActivity.this, R.string.sensorEncontrado, Toast.LENGTH_SHORT).show();
                criaPainelAutenticacao();
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                //Caso não possua o sensor
                Toast.makeText(MainActivity.this, R.string.sensorNaoEncontrado, Toast.LENGTH_SHORT).show();
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                //Caso possua o sensor mas não está disponível no momento
                Toast.makeText(MainActivity.this, R.string.sensorIndisponivel, Toast.LENGTH_SHORT).show();
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                //Caso possua o sensor mas não realizou o cadastro de uma digital
                Toast.makeText(MainActivity.this, R.string.sensorEncontradoNaoCadastrado, Toast.LENGTH_SHORT).show();
                //Abre a tela para requisitar que a pessoa cadastre uma digital
                final Intent telaConfiguracaoDigital = new Intent(Settings.ACTION_BIOMETRIC_ENROLL);
                telaConfiguracaoDigital.putExtra(Settings.EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED,BIOMETRIC_STRONG);
                startActivityForResult(telaConfiguracaoDigital, 1000);
                break;
        }
    }

    private void criaPainelAutenticacao(){
        //Objeto da classe Executor para criar e gerenciar Threads paralelas
        Executor executor = ContextCompat.getMainExecutor(this);

        //Configuração da janela popup que irá requisitar a leitura da impressão digital
        BiometricPrompt.PromptInfo telaMensagem = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Validação biométrica") //Título
                .setSubtitle("Para usar o aplicativo é necessário validar a sua impressão digital") //Mensagem do corpo
                .setAllowedAuthenticators(BIOMETRIC_STRONG | DEVICE_CREDENTIAL) //Métodos de autenticação permitidos
                .build();

        //Configuração das possiveis saídas da leitura, ou seja, com sucesso, erro ou falha
        BiometricPrompt leituraImpressao = new BiometricPrompt(MainActivity.this, executor, new BiometricPrompt.AuthenticationCallback() {
            //Caso ocorra um erro ao autenticar como, por exemplo, cancelar a tela, tentar várias vezes
            //com uma digital não cadastrada
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                //Exibe mensagem de erro
                Toast.makeText(MainActivity.this, R.string.erroLeitura, Toast.LENGTH_SHORT).show();
                //Encerra (fecha) a tela (activity)
                finish();
            }

            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                //Exibe mensagem de leitura válida
                Snackbar.make(findViewById(R.id.tela), R.string.acertoLeitura, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                //Exibe mensagem de leitura com erro
                Toast.makeText(MainActivity.this, R.string.falhaLeitura, Toast.LENGTH_SHORT).show();

            }
        });

        //Exibe a tela para realizar a leitura da impressão digital
        leituraImpressao.authenticate(telaMensagem);
    }
}